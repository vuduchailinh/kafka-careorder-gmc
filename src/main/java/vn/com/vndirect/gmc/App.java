package vn.com.vndirect.gmc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import java.util.Optional;

@SpringBootApplication
@PropertySources({
		@PropertySource({"classpath:log4j2.yml"}),
		@PropertySource(value = "file:config/application.properties", ignoreResourceNotFound = true),
})
public class App {

	private static final Logger LOG = LogManager.getLogger(App.class);

	public static void main(String[] args) {
		Optional<String> version = Optional.ofNullable(App.class.getPackage().getImplementationVersion());
		LOG.info(String.format("Noti-Worker %s starting...", version.orElse("")));
		SpringApplication.run(App.class, args);
		LOG.info(String.format("Noti-Worker %s started", version.orElse("")));
	}
}
