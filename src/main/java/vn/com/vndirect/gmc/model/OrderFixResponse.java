package vn.com.vndirect.gmc.model;

import org.springframework.beans.BeanUtils;
import vn.com.vndirect.dstream.response.OrderResponse;

import java.util.ArrayList;
import java.util.List;

public class OrderFixResponse {
    private String execType;
    private long updatedTime;
    private String orderId;
    private String account;
    private String side;
    private String orderType;
    private int quantity;
    private String symbol;
    private double price;
    private double executedPrice;
    private boolean forcedSell;
    private double averagePrice;
    private int executedQuantity;
    private long placedTime;
    private long expiredDate;
    private String status;
    private int remainingQuantity;
    private List<OrderResponse> orderReports = new ArrayList();
    private String user;
    private String via;
    private int filledQuantity;
    private String type = "NORMAL";
    private String errorCode;
    private long offset;
    private String updateTimeStr;
    private String placedTimeStr;

    public OrderFixResponse() {
    }

    public OrderFixResponse(OrderResponse order, String updateTimeStr, String placedTimeStr) {
        BeanUtils.copyProperties(order, this);
        this.updateTimeStr = updateTimeStr;
        this.placedTimeStr = placedTimeStr;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSymbol() {
        return this.symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getExecutedQuantity() {
        return this.executedQuantity;
    }

    public void setExecutedQuantity(int executedQuantity) {
        this.executedQuantity = executedQuantity;
    }

    public long getPlacedTime() {
        return this.placedTime;
    }

    public void setPlacedTime(long placedTime) {
        this.placedTime = placedTime;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getRemainingQuantity() {
        return this.remainingQuantity;
    }

    public void setRemainingQuantity(int remainingQuantity) {
        this.remainingQuantity = remainingQuantity;
    }

    public long getExpiredDate() {
        return this.expiredDate;
    }

    public void setExpiredDate(long expiredDate) {
        this.expiredDate = expiredDate;
    }

    public String getOrderId() {
        return this.orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getAccount() {
        return this.account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getSide() {
        return this.side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getOrderType() {
        return this.orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public int getFilledQuantity() {
        return this.filledQuantity;
    }

    public void setFilledQuantity(int filledQuantity) {
        this.filledQuantity = filledQuantity;
    }

    public double getExecutedPrice() {
        return this.executedPrice;
    }

    public void setExecutedPrice(double executedPrice) {
        this.executedPrice = executedPrice;
    }

    public double getAveragePrice() {
        return this.averagePrice;
    }

    public void setAveragePrice(double averagePrice) {
        this.averagePrice = averagePrice;
    }

    public boolean isForcedSell() {
        return this.forcedSell;
    }

    public void setForcedSell(boolean forcedSell) {
        this.forcedSell = forcedSell;
    }

    public String getExecType() {
        return this.execType;
    }

    public void setExecType(String execType) {
        this.execType = execType;
    }

    public void addReport(OrderResponse result) {
        this.orderReports.add(result);
    }

    public List<OrderResponse> getOrderReports() {
        return this.orderReports;
    }

    public void setOrderReports(List<OrderResponse> orderReports) {
        this.orderReports = orderReports;
    }

    public String getVia() {
        return this.via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public long getUpdatedTime() {
        return this.updatedTime;
    }

    public void setUpdatedTime(long updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public long getOffset() {
        return this.offset;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }

    public String getUpdateTimeStr() {
        return updateTimeStr;
    }

    public void setUpdateTimeStr(String updateTimeStr) {
        this.updateTimeStr = updateTimeStr;
    }

    public String getPlacedTimeStr() {
        return placedTimeStr;
    }

    public void setPlacedTimeStr(String placedTimeStr) {
        this.placedTimeStr = placedTimeStr;
    }


    public String toString() {
        return "OrderResponse [execType=" + this.execType + ", updatedTime=" + this.updatedTime + ", orderId=" + this.orderId + ", account=" + this.account + ", side=" + this.side + ", orderType=" + this.orderType + ", quantity=" + this.quantity + ", symbol=" + this.symbol + ", price=" + this.price + ", executedPrice=" + this.executedPrice + ", forcedSell=" + this.forcedSell + ", averagePrice=" + this.averagePrice + ", executedQuantity=" + this.executedQuantity + ", placedTime=" + this.placedTime + ", expiredDate=" + this.expiredDate + ", status=" + this.status + ", remainingQuantity=" + this.remainingQuantity + ", orderReports=" + this.orderReports + ", user=" + this.user + ", via=" + this.via + ", filledQuantity=" + this.filledQuantity + ", type=" + this.type + ", errorCode=" + this.errorCode + ", offset=" + this.offset + ", updateTimeStr=" + this.updateTimeStr + ", placedTimeStr=" + this.placedTimeStr + "]";
    }


}
