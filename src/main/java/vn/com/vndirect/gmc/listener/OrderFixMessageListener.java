package vn.com.vndirect.gmc.listener;

import org.springframework.stereotype.Component;
import vn.com.vndirect.dstream.client.OrderService;
import vn.com.vndirect.dstream.handler.DStreamHandler;
import vn.com.vndirect.dstream.response.OrderResponse;
import vn.com.vndirect.gmc.model.OrderFixResponse;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

@Component
public class OrderFixMessageListener extends AbstractListener {

    private OrderService orderService;

    @PostConstruct
    public void init() {
        LOGGER.info("Start listen Order fix from kafka");
        orderService = new OrderService();

        orderService.onMessage(new DStreamHandler<OrderResponse>() {
            @Override
            public void onHandler(OrderResponse order) {
                if(order.getStatus().equals(filled) || order.getStatus().equals(partiallyFilled)) {
                    OrderFixResponse orderFixResponse = convertToOrderFixResponse(order);
                    createJsonFile(orderFixResponse, orderFixResponse.getOrderId() + UUID.randomUUID());
                }
            }

        });
        orderService.start();
        LOGGER.info("Start listen kafka done!");
    }

    private OrderFixResponse convertToOrderFixResponse(OrderResponse order) {
        order.setOrderReports(Collections.emptyList());
        return new OrderFixResponse(order, df.format(new Date(order.getUpdatedTime())), df.format(new Date(order.getPlacedTime())));
    }

}
