package vn.com.vndirect.gmc.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

public abstract class AbstractListener {

    @Value("${path_file}")
    protected String pathFile;
    @Value("${order.status.filled}")
    protected String filled;
    @Value("${order.status.partiallyFilled}")
    protected String partiallyFilled;

    protected static Logger LOGGER;
    protected static DateFormat df;

    public AbstractListener() {
        LOGGER = LoggerFactory.getLogger(getClass());
        df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS a", new Locale("vi", "VN"));
    }

    protected void createJsonFile(Object data, String fileName) {
        try {
            String pathFile = createDirectoryIfNotExist(this.pathFile);

            ObjectMapper mapper = new ObjectMapper();
            mapper.setDateFormat(df);
            mapper.writeValue(new File(pathFile + fileName + ".json"), data);

            LOGGER.info("Created file: {}, data: {}", fileName, mapper.writeValueAsString(data));
        } catch(Exception ex) {
            LOGGER.error("Error in JsonFileWriter !!!");
            ex.printStackTrace();
        }
    }

    protected String createDirectoryIfNotExist(String pathFile) {
        File directory = new File(pathFile);
        if (! directory.exists()) {
            directory.mkdir();
        }

        return pathFile;
    }

}
