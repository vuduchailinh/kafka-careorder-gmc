package vn.com.vndirect.gmc.listener;

import org.springframework.stereotype.Component;
import vn.com.vndirect.dstream.client.CareOrderService;
import vn.com.vndirect.dstream.handler.DStreamHandler;
import vn.com.vndirect.dstream.response.CareOrderResponse;

import javax.annotation.PostConstruct;
import java.util.UUID;

@Component
public class CareOrderMessageListener extends AbstractListener {

    private CareOrderService careOrderService;

    @PostConstruct
    public void init() {
        LOGGER.info("Start listen Care Order from kafka");
        careOrderService = new CareOrderService();
        careOrderService.onMessage(new DStreamHandler<CareOrderResponse>() {
            @Override
            public void onHandler(CareOrderResponse order) {
                if(order.getOrderStatus().equals(filled) || order.getOrderStatus().equals(partiallyFilled))
                    createJsonFile(order, order.getOrderId() + UUID.randomUUID());
            }
        });
        careOrderService.start();
        LOGGER.info("Start listen kafka done!");
    }
}
